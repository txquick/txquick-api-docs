# Official Documentation for the TxQuick APIs and Streams.
* Official Announcements regarding changes, downtime, etc. to the API and Streams will be reported here: **https://t.me/txquick**
* Streams, endpoints, parameters, payloads, etc. described in the documents in this repository are considered **official** and **supported**.
* The use of any other streams, endpoints, parameters, or payloads, etc. is **not supported**; **use them at your own risk and with no guarantees.**

Name | Description
------------ | ------------
[rest-api.md](./rest-api.md) | Details on the Rest API (/api)
[errors.md](./errors.md) | Descriptions of possible error messages from the Rest API
[web-socket-streams.md](./web-socket-streams.md) | Details on available streams and payloads
[user-data-stream.md](./user-data-stream.md) | Details on the dedicated account stream
[wapi-api.md](./wapi-api.md) | Details on the Withdrawal API (/wapi)
[nomics-api.md](./nomics-api.md) | Details on the Nomics Exchange Integration API

## Credit to Binance & Nomics

While TxQuick could easily have used it's own API's (TxQuick has roots in BTC Trading Corp code, thus already had a very robust
API) it was decided that TxQuick should instead adopt well known API's in order to make integration quick and straightforward with existing
tools and implementations. Thus we chose to implement the Binance API for Trading and the Nomics API for Basic Market Data in lieu of continued
support for our existing API and we believe credit should go where credit is due. Thank you both Nomics (https://nomics.com/) and Binance 
(https://binance.com) for your contributions to the cryptocurrency community!

Our implementation should be compatible and if you come across any component that is not, please reach out to us on Telegram.



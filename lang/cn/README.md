# 关于本中文翻译版
* 中文文档由英文文档翻译而来，当中文文档内容与英文文档冲突时，以英文文档为准
# API文档
* API英文Telegram群 **https://t.me/txquick**
* 所有于本文档内给出定义的包括但不限于接口、数据流、参数、响应等，可认为是币安官方提供的内容。
* 而所有未于本文档内给出的内容，均不承诺提供任何支持。

文档名 | 描述
------------ | ------------
[rest-api.md](./rest-api.md) | 通用Rest接口定义 (/api)
[errors.md](./errors.md) | 错误代码及含义
[web-socket-streams.md](./web-socket-streams.md) | 行情数据流接口的描述
[user-data-stream.md](./user-data-stream.md) | 用户数据流接口的描述
[wapi-api.md](./wapi-api.md) | 提现接口(及其他账户信息)的描述(/wapi)
